package oop.asg04;
// Board.java

/**
 * CS108 Tetris Board. Represents a Tetris board -- essentially a 2-d grid of
 * booleans. Supports tetris pieces and row clearing. Has an "undo" feature that
 * allows clients to add and remove pieces efficiently. Does not do any drawing
 * or have any idea of pixels. Instead, just represents the abstract 2-d board.
 */
public class Board {
    // Some ivars are stubbed out for you:

    private int width;
    private int height;
    private boolean[][] grid;
    private boolean DEBUG = true;
    boolean committed;
    
    // 2 cau truc phu
    private int widths[];
    private int heights[];
    
    // 3 mang dung de bach up
    private int[] xWidths;
    private int[] xHeights;
    private boolean[][] xGrid;
    
    // Here a few trivial methods are provided:
    /**
     * Creates an empty board of the given width and height measured in blocks.
     */
    public Board(int width, int height) {
        
        this.width = width;
        this.height = height;
        grid = new boolean[width][height];
        for (int i = 0; i < width; i++) 
            for(int j = 0; j < height; j++){
                grid[i][j] = false;
                
            }
        
        committed = true;

        
        widths = new int[height];
        
        for (int i = 0; i < height; i++)
            widths[i] = 0;
        
        heights = new int[width];
        
        for (int i = 0; i < width; i++)
            heights[i] = 0;
        
        xWidths = new int[height];
        System.arraycopy(widths, 0, xWidths, 0, height);
        
        xHeights = new int[width];
        System.arraycopy(heights, 0, xHeights, 0, width);
        
        xGrid = new boolean[width][height];
        for (int i = 0; i < width; i++) 
            for (int j = 0; j < height; j++) {
                xGrid[i][j] = false;
            }
    }

    public boolean[][] getGrid(){
        return grid;
    }
    
    public int[] getWidths(){
        return widths;
    }
    
    public int[] getHeights(){
        return heights;
    }
    
    /**
     * Returns the width of the board in blocks.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the board in blocks.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the max column height present in the board. For an empty board
     * this is 0.
     */
    public int getMaxHeight() {
        int maxHeight = heights[0];
        for (int i = 1; i < width; i++) {
            if (heights[i] > maxHeight) {
                maxHeight = heights[i];
            }
        }
        return maxHeight; 
        
    }

    /**
     * Checks the board for internal consistency -- used for debugging.
     */
    public void sanityCheck() {
        int[] c_widths = new int[height];
        int[] c_heights = new int[width];
        int c_maxHeight = 0;
        int c_count = 0;
        if (DEBUG) {
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    if(grid[i][j])
                        c_count = j+1;
                }
                if(c_maxHeight < c_count)
                    c_maxHeight = c_count;
                
                c_heights[i] = c_count;
                c_count = 0;
                
            }
            c_count = 0;
            
            for(int j = 0; j < height; j++) {
                for (int i = 0; i < width ; i++) {
                    if(grid[i][j])
                        c_count++;
                }
                c_widths[j] = c_count;
                c_count = 0;
            }
            for (int i = 0; i < width; i++) {
                if(c_heights[i] != heights[i]){
                    System.out.println("Heights : " + i);
                    System.out.println("heights : " + heights[i]);
                    System.out.println("c_Heights : " + c_heights[i]);
                   throw new RuntimeException("error heights \n"); 
                   
                }   
            }
            
            for (int i = 0; i < height; i++) {
                if(c_widths[i] != widths[i])
                   throw new RuntimeException("error widths \n"); 
                   
             
            }
            if(c_maxHeight != getMaxHeight()){
                System.out.println("c_maxHeight : " + c_maxHeight);
                System.out.println("maxHeight : " + getMaxHeight());
                throw new RuntimeException("error max height \n"); 
                   
            }
        }
    }

    /**
     * Given a piece and an x, returns the y value where the piece would come to
     * rest if it were dropped straight down at that x.
     *
     * <p>
     * Implementation: use the skirt and the col heights to compute this fast --
     * O(skirt length).
     */
    public int dropHeight(Piece piece, int x) {
        int maxHeight = 0;
        int maxArr = heights[x];
        int pieceHeight = piece.getHeight();
        int index = x;
        int i;
        int[] pSkirt = piece.getSkirt();
        for (i = x + 1; i < x + piece.getWidth(); i++) {
            if(maxArr < heights[i]){
                maxArr = heights[i];
                index = i;
            }
        }
       
       if(heights[index] - heights[x]  < pSkirt[index - x]){
           
           maxHeight = heights[x] - pSkirt[0];
           if(maxHeight < 0){
               maxHeight = 0;
           }
       }
       else{
          
           maxHeight = heights[index] - pSkirt[index - x] ;
       }
    
        return maxHeight;
    }

    /**
     * Returns the height of the given column -- i.e. the y value of the highest
     * block + 1. The height is 0 if the column contains no blocks.
     */
    public int getColumnHeight(int x) {
        return heights[x]; 
    }

    /**
     * Returns the number of filled blocks in the given row.
     */
    public int getRowWidth(int y) {
        return widths[y]; 
    }

    /**
     * Returns true if the given block is filled in the board. Blocks outside of
     * the valid width/height area always return true.
     */
    public boolean getGrid(int x, int y) {
        if(x >= width || x < 0|| y >= height || y < 0 || grid[x][y] )
            return true;
        return false; 
    }
    public static final int PLACE_OK = 0;
    public static final int PLACE_ROW_FILLED = 1;
    public static final int PLACE_OUT_BOUNDS = 2;
    public static final int PLACE_BAD = 3;

    /**
     * acement may fail in two ways. First, if part of the
     * piece may falls out of bounds of the board, PLACE_OUT_BOUNDS is returned.
     * Or the placement may collide with existing blocks in the grid in which
     * case PLACE_BAD is returned. In both error cases, the board may be left in
     * an invalid state. The client can use undo(), to recover the valid,
     * pre-place state.
     */
    public int place(Piece piece, int x, int y) {
        // flag !committed problem
        if (!committed) {
            throw new RuntimeException("place commit problem");
        }
        
        // copy sang cac mang backup
        System.arraycopy(widths, 0, xWidths, 0, height);
        System.arraycopy(heights, 0, xHeights, 0, width);
        for (int i = 0; i < width; i++) 
            for (int j = 0; j < height; j++) {
                xGrid[i][j] = grid[i][j];
            }
        
        // tim ket qua cho ham place()
        int result = PLACE_OK;
       
        
        
        // dat Piece ngoai Board
        if(x < 0 || y < 0 || x +  piece.getWidth() > width || y + piece.getHeight() > height){
    
             result = PLACE_OUT_BOUNDS;
        }
        else{ // dat Piece vao o da dc to trong Board
             TPoint[] p_body;
             p_body = piece.getBody();
             int[] p_skirt = piece.getSkirt();
             int i  = 0;
             while( i < 4){
                 if(grid[x + p_body[i].x][y + p_body[i].y]){
                      
                      result = PLACE_BAD;
                      break;
                 }
                i++;
            }
            if(result != PLACE_BAD){
                for ( i = 0; i < 4; i++) {
                    // to cac o 
                    grid[x + p_body[i].x][y + p_body[i].y] = true;
                    
                     //    tinh cac phan tu cua mang width[]
                    widths[y + p_body[i].y]++;
                    
                    //    tinh cac phan tu cua mang heights[]
                    if(heights[x + p_body[i].x] < y + p_body[i].y + 1){
                        heights[x + p_body[i].x] = y + p_body[i].y + 1;
                    }
                    
                }
                i = y;
                //kiem tra xem cac hang ma Piece duoc dat co day hang khong
                while(i < (y + piece.getHeight())){
                    if(widths[i] == width)
                        result = PLACE_ROW_FILLED;
                
                    i++;
                }
                
            }
        }
        
        
        
        this.sanityCheck();
        committed = false;
        return result;
    }

    /**
     * Deletes rows that are filled all the way across, moving things above
     * down. Returns the number of rows cleared.
     */
    public int clearRows() {
            
        int rowsCleared = 0;
        int index = 0;
        int i = 0;
        
        while(widths[index] != 0 && index < height){
            if(widths[index] == width){
                i = index;
                
                // copy dong tren xuong dong duoi trong grid[][] va widths[]
                while(widths[i] != 0 && (i + 1) < height){
                    for (int j = 0; j < width; j++) {
                        grid[j][i] = grid[j][i+1];
                    }
                    
                    widths[i] = widths[i+1];
                    i++;
                }
                
                // dong tren deu la false
                widths[i] = 0;
                for (int j = 0; j < width; j++) {
                    grid[j][i] = false;
                }
                // tinh lai heights[]
                for(int k = 0; k < width;k++){
                    heights[k] = 0;
                    for (int j = i - 1; j >= 0;j--){
                        if(grid[k][j]){
                            heights[k] = j + 1 ;
                            break;
                        }
                        
                    }
                }
                rowsCleared++;
                
            }
            else{
                index++;
                if(index == height){
                    break;
                }
            }
            
            
        }
        
        this.sanityCheck();
        committed = false;
        return rowsCleared;
    }

    /**
     * Reverts the board to its state before up to one place and one
     * clearRows(); If the conditions for undo() are not met, such as calling
     * undo() twice in a row, then the second undo() does nothing. See the
     * overview docs.
     */
    public void undo() {
        if(!committed){
            int[] temp = widths;
                    
            widths = xWidths;
            xWidths = temp;
            
            temp = heights;
            heights = xHeights;
            xHeights = temp;
            
            boolean[][] temp2 = grid;
            grid = xGrid;
            xGrid = temp2;
            
            commit();
            this.sanityCheck();
        }
       
    }

    /**
     * Puts the board in the committed state.
     */
    public void commit() {
        committed = true;
    }

    /*
     Renders the board state as a big String, suitable for printing.
     This is the sort of print-obj-state utility that can help see complex
     state change over time.
     (provided debugging utility) 
     */
    public String toString() {
        StringBuilder buff = new StringBuilder();
        for (int y = height - 1; y >= 0; y--) {
            buff.append('|');
            for (int x = 0; x < width; x++) {
                if (getGrid(x, y)) {
                    buff.append('+');
                } else {
                    buff.append(' ');
                }
            }
            buff.append("|\n");
        }
        for (int x = 0; x < width + 2; x++) {
            buff.append('-');
        }
        return (buff.toString());
    }

    private void commited() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
