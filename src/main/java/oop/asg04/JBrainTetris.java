/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oop.asg04;

import javax.swing.JComponent;
import java.awt.*;
import javax.swing.*;

import java.util.*;
import java.awt.event.*;
import javax.swing.event.*;

import java.awt.Toolkit;

/**
 *
 * @author dat
 */

public class JBrainTetris extends JTetris{
    private DefaultBrain defaBrain;
    public boolean testbrain = false;
    protected JCheckBox brainMode;
    
    JBrainTetris(int pixels ){
        super(pixels);
    }
   
    public void tick(int verb){
        if(verb == DOWN){
            // choi tu dong
                Random rand = new Random();
                boolean randRotation = rand.nextBoolean();
                int     randMove     = rand.nextInt(3);
                if(randRotation){
                    verb = ROTATE;
                }
                
                switch(randMove){
                    case 1 : verb = LEFT; break;
                    case 2 : verb = RIGHT; break;
                }
        }
        super.tick(verb);
		
    }
    public JComponent createControlPanel(){
        super.createControlPanel();
        JComponent panel = super.createControlPanel();    ;
        panel.add(new JLabel("Brain:"));
        brainMode = new JCheckBox("Brain active");
        panel.add(brainMode);
        return panel;
    }
    public static void main(String[] args){
        try {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Exception ignored) { }
		
		JBrainTetris tetris = new JBrainTetris(16);
		JFrame frame = JTetris.createFrame(tetris);
		frame.setVisible(true);
    }
}
