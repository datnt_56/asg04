package oop.asg04;
import static org.junit.Assert.*;

import org.junit.*;

public class BoardTest {
	Board b;
	Piece pyr1, pyr2, pyr3, pyr4, s, sRotated;

	// This shows how to build things in setUp() to re-use
	// across tests.
	
	// In this case, setUp() makes shapes,
	// and also a 3X6 board, with pyr placed at the bottom,
	// ready to be used by tests.
	@Before
	public void setUp() throws Exception {
		b = new Board(3, 6);
		
		pyr1 = new Piece(Piece.PYRAMID_STR);
		pyr2 = pyr1.computeNextRotation();
		pyr3 = pyr2.computeNextRotation();
		pyr4 = pyr3.computeNextRotation();
		
		s = new Piece(Piece.S1_STR);
		sRotated = s.computeNextRotation();
		
		b.place(pyr1, 0, 0);
	}
	
	// Check the basic width/height/max after the one placement
	@Test
	public void testSample1() {
		assertEquals(1, b.getColumnHeight(0));
		assertEquals(2, b.getColumnHeight(1));
		assertEquals(2, b.getMaxHeight());
		assertEquals(3, b.getRowWidth(0));
		assertEquals(1, b.getRowWidth(1));
		assertEquals(0, b.getRowWidth(2));
	}
	
	// Place sRotated into the board, then check some measures
        
	@Test
	public void testSample2() {
		b.commit();
		int result = b.place(sRotated, 1, 1);
                
		assertEquals(Board.PLACE_OK, result);
		assertEquals(1, b.getColumnHeight(0));
		assertEquals(4, b.getColumnHeight(1));
		assertEquals(3, b.getColumnHeight(2));
		assertEquals(4, b.getMaxHeight());
	}
        @Test
        public void testSample3(){
            b.commit();
            Piece SQ = new Piece(Piece.SQUARE_STR);
            int result = b.place(SQ, 1, 2);
            assertEquals(Board.PLACE_OK, result);
            assertEquals(1, b.getColumnHeight(0));
            assertEquals(4, b.getColumnHeight(1));
            assertEquals(4, b.getColumnHeight(2));
            assertEquals(4, b.getMaxHeight());
        }
        @Test
        public void testSample4(){
            b.commit();
            Piece SQ = new Piece(Piece.SQUARE_STR);
            int result = b.place(SQ, 1, 2);
            assertEquals(Board.PLACE_OK, result);
            assertEquals(3, b.getRowWidth(0));
            assertEquals(1, b.getRowWidth(1));
            assertEquals(2, b.getRowWidth(2));
            assertEquals(2, b.getRowWidth(3));
            assertEquals(0, b.getRowWidth(4));
            assertEquals(0, b.getRowWidth(5));
            assertEquals(4, b.getMaxHeight());
        }
        @Test
        public void testContructorBoard(){
            Board board = new Board(4,5);
            assertArrayEquals(new int[]{0, 0, 0, 0}, board.getHeights());
            assertArrayEquals(new int[]{0, 0, 0, 0, 0}, board.getWidths());
            assertEquals(5, board.getHeight());
            assertEquals(4, board.getWidth());
            assertEquals(false, board.getGrid(0, 0));
            assertEquals(true, board.getGrid(-1, 0));
            assertEquals(true, board.getGrid(0, -1));
            assertEquals(true, board.getGrid(0, 6));
            assertEquals(true, board.getGrid(0, 7));
        }
        @Test
        public void testHeights(){
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            b.commit();
            Piece SQ = new Piece(Piece.SQUARE_STR);
            int result = b.place(SQ, 1, 2);
            assertArrayEquals(new int[]{1,4,4}, b.getHeights());
            
            
            
        }
        @Test
        public void testwidths(){
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
        }
        
        @Test
        public void testDropheight(){
            Piece S2 = new Piece(Piece.S2_STR);
            
            Piece S3 = S2.computeNextRotation();
            assertEquals(2,b.dropHeight(pyr1, 0));
            assertEquals(1,b.dropHeight(S3, 0));
            assertEquals(2,b.dropHeight(S3, 1));
            
            
            Board c = new Board(5,7);
            
            Piece s1 = new Piece(Piece.S1_STR);
            assertEquals(0, c.dropHeight(s1, 0));
            
            Piece l2 = new Piece(Piece.L2_STR);
            assertEquals(0, c.dropHeight(l2, 0));
        }
        
        @Test
        public void testDropHeight2(){
            Board c = new Board(9, 15);
            Piece s1 = new Piece(Piece.S1_STR);
            Piece l2 = new Piece(Piece.L2_STR);
            Piece l2_r = l2.computeNextRotation();
            Piece l2_2r = l2_r.computeNextRotation();
            
            c.place(pyr1, 2, 0);
            c.commit();
            c.place(s1, 3, 2);
            c.commit();
            assertEquals(1,c.getColumnHeight(2));
            assertEquals(3,c.getColumnHeight(3));
            assertEquals(4,c.getColumnHeight(4));
            assertEquals(4,c.getColumnHeight(5));
            
            assertEquals(3,c.dropHeight(l2_2r, 3));
            
        }
         @Test
        public void testDropHeight3(){
            Board c = new Board(9, 15);
            Piece s1 = new Piece(Piece.S1_STR);
            Piece s2 = new Piece(Piece.S2_STR);
            Piece l2 = new Piece(Piece.L2_STR);
            Piece l2_r = l2.computeNextRotation();
            Piece l2_2r = l2_r.computeNextRotation();
            
            c.place(pyr1, 2, 0);
            c.commit();
            c.place(s1, 3, 2);
            c.commit();
            assertEquals(1,c.getColumnHeight(2));
            assertEquals(3,c.getColumnHeight(3));
            assertEquals(4,c.getColumnHeight(4));
            assertEquals(4,c.getColumnHeight(5));
            
            assertEquals(3,c.dropHeight(l2_2r, 3));
            
        }
         @Test
         public void testDropHeight4(){
             Board c = new Board(9, 15);
             Piece st = new Piece(Piece.STICK_STR);
             c.place(st, 3, 0);
             c.commit();
             c.place(st, 3, 4);
             c.commit();
             c.place(s, 4, 0);
             c.commit();
             
             
             assertEquals(7, c.dropHeight(pyr2, 3));
             
             
         }
        @Test
        public void testPLACE_OUT_BOUNDS(){
            
            // test bien phai
            b.commit();
            assertEquals(Board.PLACE_OUT_BOUNDS, b.place(pyr2, -1, 0));
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            assertEquals(false, b.committed);
            
            // test bien duoi
            b.commit();
            assertEquals(Board.PLACE_OUT_BOUNDS, b.place(pyr2, 1, -3));
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            assertEquals(false, b.committed);
            
            // test bien ben trai
            b.commit();
            assertEquals(Board.PLACE_OUT_BOUNDS, b.place(pyr2, 3, 3));
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            assertEquals(false, b.committed);
            
            // test bien ben tren
            b.commit();
            assertEquals(Board.PLACE_OUT_BOUNDS, b.place(pyr2, 2, 6));
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            assertEquals(false, b.committed);
            
            // 1 nua o ngoai 1 nua o trong Board
            b.commit();
            assertEquals(Board.PLACE_OUT_BOUNDS, b.place(pyr2, 0, 5));
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            assertEquals(false, b.committed);
            
        }
        @Test
        public void testPLACE_BAB(){
            b.commit();
            Piece l = new Piece(Piece.L1_STR);
            assertEquals(Board.PLACE_BAD, b.place(l, 0, 1));
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            assertEquals(false, b.committed);
        }
        
	@Test
        public void testPLACE_ROW_FILLED(){
            b.commit();
            //Piece l = new Piece(Piece.L1_STR);
            assertEquals(Board.PLACE_ROW_FILLED, b.place(pyr1, 0, 2));
            assertEquals(3, b.getColumnHeight(0));
            assertEquals(4, b.getColumnHeight(1));
            assertEquals(3, b.getColumnHeight(2));
            assertEquals(4, b.getMaxHeight());
            assertArrayEquals(new int[]{3,1,3,1,0,0}, b.getWidths());
            assertArrayEquals(new int[]{3,4,3}, b.getHeights());
            assertEquals(false, b.committed);
            
        }
        
        @Test
        public void testClearRows(){
            assertEquals(1,b.clearRows());
            assertArrayEquals(new int[]{1,0,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{0,1,0}, b.getHeights());
            assertEquals(false, b.committed);
            b.commit();
            b.place(pyr1, 0, 1);
            assertEquals(1,b.clearRows());
            assertEquals(0, b.getColumnHeight(0));
            assertEquals(2, b.getColumnHeight(1));
            assertEquals(0, b.getColumnHeight(2));
            assertEquals(false, b.committed);
            //System.out.print(b);
            
        }
        @Test
        public void testClearTwoRows(){
            Board c = new Board(3,6);
            c.commit();
            c.place(pyr1, 0, 0);
            c.commit();
            Piece SQ = new Piece(Piece.SQUARE_STR);
            c.place(SQ, 0, 2);
            c.commit();
            c.place(pyr1, 0, 4);
            
            assertEquals(2,c.clearRows());
            assertEquals(3, c.getColumnHeight(0));
            assertEquals(4, c.getColumnHeight(1));
            assertEquals(0, c.getColumnHeight(2));
            assertEquals(false, c.committed);
            
        }
        @Test
        public void testClearThreeRows(){
            b.commit();
            b.place(pyr1, 0, 2);
            b.commit();
            b.place(pyr1, 0, 4);
            assertEquals(3, b.clearRows());
        }
        @Test
        public void testClearFourRows(){
            Board c = new Board(3,6);
            Piece SQ = new Piece(Piece.SQUARE_STR);
            Piece tick = new Piece(Piece.STICK_STR);
            c.commit();
            c.place(SQ, 0, 0);
            c.commit();
            c.place(SQ, 0, 2);
            c.commit();
            c.place(tick, 2, 0);
            assertEquals(4, c.clearRows());
            assertEquals(0, c.getColumnHeight(0));
            assertEquals(0, c.getColumnHeight(1));
            assertEquals(0, c.getColumnHeight(2));
            assertEquals(false, c.committed);
            
        }
        @Test
        public void testUndo(){
            assertArrayEquals(new int[]{3,1,0,0,0,0}, b.getWidths());
            assertArrayEquals(new int[]{1,2,1}, b.getHeights());
            b.undo();
            //b.clearRows();
            //b.commit();
            b.place(pyr1, 0, 0);
            b.undo();
            Board c = new Board(3,6);
            assertArrayEquals(c.getWidths(),b.getWidths());
            assertArrayEquals(c.getHeights(),b.getHeights());
            assertArrayEquals(c.getGrid(),b.getGrid());
            assertEquals(true, b.committed);
            
        }

	// Make  more tests, by putting together longer series of 
	// place, clearRows, undo, place ... checking a few col/row/max
	// numbers that the board looks right after the operations.
	
	
}
